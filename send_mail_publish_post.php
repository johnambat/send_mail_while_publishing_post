<?php
/**
 * Plugin Name:send mail while publishing post 
 * Plugin URI: http://www.avaninfotech.com
 * Description: This plugin sends mail to the author when a post is published.
 * Version: 1.0.0
 * Author: John SImon 
 * Author URI: http://www.avaninfotech.com
 * License: GPL2
 */

add_action( 'publish_post', 'post_published_notification', 10, 2 );
function post_published_notification( $ID, $post ) {
    $email = get_the_author_meta( 'user_email', $post->post_author );
    $subject = 'Published ' . $post->post_title;
    $message = 'We just published your post: ' . $post->post_title . ' take a look: ' . get_permalink( $ID ); 
    wp_mail( $email, $subject, $message );
}

?>
