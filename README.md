==send mail while publishing post==
Contributors: John Simon

Tags: publish post, send mail, author
Requires at least: 3.5.1
Tested up to: 4.1.1
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
 
Organized documentation for your digital products
 
== Description ==
 
send mail to the author when the admin publish the post
= Plugin Features =
 
*   send mail to the author

== Installation ==
 
1.  Upload your plugin folder to the '/wp-content/plugins' directory.
2.  Activate the plugin through the 'Plugins' menu in WordPress.
 
== Frequently Asked Questions ==
 
There are no FAQ just yet.
 
== Changelog ==
 
= 1.0.0 =
*   First release
 
== Upgrade Notice ==
 
There is no need to upgrade just yet.
 
== Screenshots ==
 
There are no screenshots yet.